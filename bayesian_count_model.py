import numpy as np
try:
    import TECAMERRAARCounter
except:
    import warnings
    warnings.warn("Failed to import TECAMERRAARCounter.")

parameter_bounds = [ \
                    [0.8,0.99], \
                    [5, 25], \
                    [1e11, 5e12], \
                   ]

# initialize the AR Detector
ar_detector = None #TECAMERRAARCounter.TECAMERRAARCounter(ar_count_regex, no_time_var = False)

def log_likelihood(theta, N, use_sigma = False, count_array = None):
    """ Calculates the log likelihood of the observed AR counts given the detected AR counts. """
    
    # extract the model parameters
    if use_sigma:
        percentile, filter_lat_width, minimum_area, sigma = theta
    else:
        percentile, filter_lat_width, minimum_area = theta
        sigma = 2.0


    # get AR counts
    Np = ar_detector.count(percentile, filter_lat_width, minimum_area)

    if count_array is not None:
        try:
            count_array[:] = np.array(Np)
        except:
            pass

    # if any counts are 0, return log(L) = 0 to rule out that parameter setting
    if any([ ll == 0 for ll in Np ]):
        L = -np.inf
    else:
        # calculate the log likelihood
        L = np.sum(-0.5*((N - Np)/sigma)**2) - len(Np)*np.log(sigma*np.sqrt(2*np.pi))
    
    return L
    
    
def log_prior(theta, use_sigma = False):
    """ Returns the log of the prior """
    
    # extract the model parameters
    if use_sigma:
        percentile, filter_lat_width, minimum_area, sigma = theta
    else:
        percentile, filter_lat_width, minimum_area = theta
        
    
    percentile_bounds = parameter_bounds[0]
    filter_lat_width_bounds = parameter_bounds[1]
    minimum_area_bounds = parameter_bounds[2]
    
    # initialize the log prior
    log_P = 0
    
    # add contributions from each of the uniform distribution terms
    for param, param_bounds in zip([percentile, filter_lat_width, minimum_area], [percentile_bounds, filter_lat_width_bounds, minimum_area_bounds]):
        log_P += np.log(1/(param_bounds[1] - param_bounds[0]))
            
    if use_sigma:
        # add the contribution from the sigma term
        s = 10
        log_P += np.log((2/(np.pi*s))*(1/(1 + (sigma/s)**2)))
        
    return log_P
        
    
def check_parameter_combo_validity(theta, Abar, Nt, use_sigma):
    """ Checks whether the parameter combinations are valid; return False if not. """

    parameter_combo_is_valid = True

    # extract the model parameters
    if use_sigma:
        percentile, filter_lat_width, minimum_area, sigma = theta
    else:
        percentile, filter_lat_width, minimum_area = theta

    # calculate the maximum area beyond which the prior is automatically 0
    Amax = Abar*(1 - np.sin(filter_lat_width*np.pi/180))*Nt/10*(1-percentile)

    # if the minimum area is greater than the max, then flag that this parameter combo is bad
    if minimum_area > Amax:
        parameter_combo_is_valid = False

    # check parameter bounds
    for param, param_bounds in zip([percentile, filter_lat_width, minimum_area], parameter_bounds):
        if param < param_bounds[0] or param > param_bounds[1]:
            parameter_combo_is_valid = False

    if use_sigma:
        if sigma <= 0:
            parameter_combo_is_valid = False

    return parameter_combo_is_valid

    
def log_posterior(theta, N, use_sigma, Abar, Nt, count_array = None):
    """ Calculates the log posterior of the observed AR counts given the detected AR counts. """

    # if the parameter combination is not valid, automatically set the posterior to 0
    if not check_parameter_combo_validity(theta, Abar, Nt, use_sigma):
        L = -np.inf
    else:
        L = 0

    # calculate the log prior
    if L != -np.inf:
        L = log_prior(theta, use_sigma = use_sigma)

    # deal with nan -- flag that this parameter setting is bad
    if np.isnan(L):
        L = -np.inf

    # only run the expensive model if the parameter setting isn't a priori bad
    if L != -np.inf:
        # run the likelihood model
        L += log_likelihood(theta,N,use_sigma = use_sigma, count_array = count_array)
    else:
        # if we didn't call the model, attempt to set the counts for this parameter setting to NaN
        if count_array is not None:
            try:
                count_array[:] = np.nan
            except:
                pass

    # deal with nan -- flag that this parameter setting is bad
    if np.isnan(L):
        L = -np.inf


    return L
