import teca
import numpy as np
import numba

class TECAMERRAARCounter:
    """ Counts ARs in MERRA output. This is designed to be run repeatedly after updates to the parameter inputs (i.e., MCMC).
    
        This pipeline is based explicitly on the pipeline defined in TECA/alg/teca_bayesian_ar_detect.cxx at revision 2104b37.
        
        The pipeline is reconstructed and modified here in order to obtain the counts of ARs in a batch of input files.
    
    """
    
    def __init__(self,
                  mesh_data_regex = "MERRA_test/ARTMIP_MERRA_2D_2017021.*\.nc$", \
                  water_vapor_variable = "IVT",
                  no_time_var = True,
                  comm = None,
                  thread_pool_size = -1,
                 ):
            
        # store the input arguments
        self.water_vapor_variable = water_vapor_variable
        self.mesh_data_regex = mesh_data_regex
    
        #*****************************
        # create the reader component
        #*****************************
        self.mesh_data_reader = teca.teca_cf_reader.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.mesh_data_reader.set_communicator(comm)
        # set the filenames
        self.mesh_data_reader.set_files_regex(mesh_data_regex)
        # indicate that longitude is periodic
        self.mesh_data_reader.set_periodic_in_x(1)
        # set the lat/lon variables
        self.mesh_data_reader.set_x_axis_variable('lon')
        self.mesh_data_reader.set_y_axis_variable('lat')
        if no_time_var:
            # indicate that there is no time variable
            self.mesh_data_reader.set_t_axis_variable('')
        else:
            # set the time variable
            self.mesh_data_reader.set_t_axis_variable('time')
        self.mesh_data_reader.set_thread_pool_size(1)



        # normalize the input data
        self.norm = teca.teca_normalize_coordinates.New()
        self.norm.set_input_connection(self.mesh_data_reader.get_output_port())

        #**************************************
        # create the latitude damper component
        #**************************************
        self.damp = teca.teca_latitude_damper.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.damp.set_communicator(comm)
        # connect it to the reader
        self.damp.set_input_connection(self.norm.get_output_port())
        # set the variable for damping
        self.damp.set_damped_variables([water_vapor_variable])

        #**************************************
        # create the segmentation component
        #**************************************
        self.seg = teca.teca_binary_segmentation.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.seg.set_communicator(comm)
        # connect it to the latitude dampber
        self.seg.set_input_connection(self.damp.get_output_port())
        # set the variable work on
        self.seg.set_threshold_variable(water_vapor_variable)
        # set the name of the output field of this component
        self.seg.set_segmentation_variable("wv_seg")
        # indicate that we are thresholding by percentile rather than absolute value
        self.seg.set_threshold_by_percentile()

        #******************************************
        # create the connected component algorithm
        #******************************************
        self.cc = teca.teca_connected_components.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.cc.set_communicator(comm)
        # connect it to the segmentation algorithm
        self.cc.set_input_connection(self.seg.get_output_port())
        # set the variable name from the segmentation algorithm
        self.cc.set_segmentation_variable("wv_seg")
        # set the name of the output field
        self.cc.set_component_variable("wv_cc")

        #**************************************
        # create the component area calculator
        #**************************************
        self.ca = teca.teca_2d_component_area.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.ca.set_communicator(comm)
        # connect it to the connected-component algorithm
        self.ca.set_input_connection(self.cc.get_output_port())
        # set the variable name from the connected-component algorithm
        self.ca.set_component_variable("wv_cc")
        # overwrite the component IDs with areas
        # TODO: check whether that's what this is doing
        self.ca.set_contiguous_component_ids(1)

        #**********************************
        # create the component area filter
        #**********************************
        self.caf = teca.teca_component_area_filter.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.caf.set_communicator(comm)
        # connect it to the component area algorithm
        self.caf.set_input_connection(self.ca.get_output_port())
        # set the variable name from the connected component algorithm
        self.caf.set_component_variable("wv_cc")
        self.caf.set_contiguous_component_ids(1)

        self.tabl = teca.teca_component_statistics.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.tabl.set_communicator(comm)
        self.tabl.set_input_connection(self.caf.get_output_port())

        #**************************************************
        # create the table reduction and sorting algorithm
        #**************************************************
        self.mr = teca.teca_table_reduce.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.mr.set_communicator(comm)
        # connect the table reduction to the connected component counter
        self.mr.set_input_connection(self.tabl.get_output_port())
        # turn on verbosity
        #self.mr.set_verbose(1)
        # indicate that the thread pool size should be automatically inferred
        self.mr.set_thread_pool_size(thread_pool_size)

        # define a table sorter
        self.ts = teca.teca_table_sort.New()
        # set the MPI communicator if one was provided
        if comm is not None:
            self.ts.set_communicator(comm)
        # connect it to the table reduction
        self.ts.set_input_connection(self.mr.get_output_port())
        # sort on the timestep, so that the areas are ordered the same as the
        # fields in the input file(s)
        self.ts.set_index_column('step')

        #************************
        # create a table-grabber
        #************************
        self.dsc = teca.teca_dataset_capture.New()
        self.dsc.set_input_connection(self.ts.get_output_port())
    
    
    def count(self,
              relative_value_threshold = 0.85,
              hwhm_latitude = 30.0,
              area_threshold = 1e12,
              filter_center = 0.0):
        """ Sets the parameters for the TECA pipeline, executes the pipline, and counts the connected components. """

        
        # set the pipeline parameters
        self.seg.set_low_threshold_value(relative_value_threshold*100)
        self.damp.set_half_width_at_half_max(hwhm_latitude)
        self.damp.set_center(filter_center)
        self.caf.set_low_area_threshold(area_threshold * 1e-6)
    
        # run the pipeline
        retval = self.dsc.update()

        component_steps = np.asarray(teca.as_teca_table(self.dsc.get_dataset()).get_column('step'), dtype = np.int64)

        # return a vector of the number of connected components in each field
        # reduce the counts by 1, since the background is counted as a component in TECA
        return get_count(component_steps) - 1

@numba.jit("int64[:](int64[:])", nopython = True)
def get_count(component_steps):
    num_steps = component_steps[-1] + 1
    counts = np.zeros(num_steps, dtype = np.int64)
    for i in range(len(component_steps)):
        counts[component_steps[i]] += 1

    return counts



if __name__ == "__main__":
    from mpi4py import MPI
    import timeit

    comm = MPI.COMM_SELF
    rank = comm.Get_rank()

    setup = \
    """
from mpi4py import MPI

comm = MPI.COMM_SELF
rank = comm.Get_rank()
from __main__ import TECAMERRAARCounter

my_counter = TECAMERRAARCounter("/dev/shm/taobrien/ar_counts_user_taobrien.nc", thread_pool_size = 1, no_time_var = False, comm = comm)
    """

    #my_counter = TECAMERRAARCounter("/dev/shm/taobrien/ar_counts_user_indah_0.*\.nc", no_time_var = True, thread_pool_size = 272)
    #my_counter = TECAMERRAARCounter("/project/projectdirs/m1517/cascade/external_datasets/ARTMIP/MERRA_2D/2017/ARTMIP_MERRA_2D_20170210_06\.nc", thread_pool_size = 1)
    #my_counter = TECAMERRAARCounter("/global/u1/t/taobrien/m1517_taobrien/teca_bayesian_ar_detector/mcmc_training/expert_count_evaluation/ar_counts_user_taobrien\.nc", thread_pool_size = 1, no_time_var = False)
    my_counter = TECAMERRAARCounter("/project/projectdirs/m1517/cascade/TECA_data/ARTMIP_MERRA_2D_2017-05-16-00Z\.nc", no_time_var = False)
    print(my_counter.count())

    profile = False
    if profile:

        teca.teca_profiler_initialize()
        reps = 10
        time = timeit.timeit("my_counter.count()", number = reps, setup = setup)
        print(f"{time/reps} seconds per call")
        #print(my_counter.count())
        teca.teca_profiler_finalize()
