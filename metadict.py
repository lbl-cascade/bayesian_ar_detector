# get any git repository info, if available
import git
import socket
import sys
import os
try:
    _repo = git.Repo(search_parent_directories=True)
    _git_sha = _repo.head.object.hexsha
    _git_short_sha = _repo.git.rev_parse(_git_sha, short=7)
    _git_branch = _repo.active_branch
except:
    print("No repository detected.")


class metadict(dict):
    """ A dict object into which we can store metadata """
    def __init__(self, *args, **kw):
        super(metadict,self).__init__(*args, **kw)

    def print_props(self):
        """ Prints stored properties of the dictionary """
        props = self.__dict__
        for key in props:
            print("\t{}:\t{}".format(key,props[key]))


def store_metadata_info(output_dictionary):
        """ Stores relevant metadata to a pickling dictionary """

        # force convert the database to a meta dictionary
        output_dictionary = metadict(output_dictionary)

        # store the git SHA
        output_dictionary.git_sha = _git_sha
        output_dictionary.git_branch = _git_branch

        # store the host name
        output_dictionary.hostname = socket.gethostname()
        output_dictionary.cwd = os.getcwd()

        # store the command used
        output_dictionary.command = sys.argv[0]

        return output_dictionary


