import pickle
import sys
import netCDF4 as nc
import numpy as np
from ARDetector import ARDetector
import bayesian_count_model as bayes
import metadict
from simplempi import simpleMPI

# initialize mpi
# if any argument is provided MPI is used
smpi = simpleMPI.simpleMPI()

if smpi.rank == 0:
    smpi.pprint("Loading AR count info")
    # load the count file
    ar_count_file = '/project/projectdirs/m1517/cascade/taobrien/artmip/tier1/ar_count_interface/ivt_rgb_images_percentile_counts.nc'
    with nc.Dataset(ar_count_file) as fin:
        IVT = fin.variables['IVT'][:]
        ar_count = fin.variables['AR_COUNT'][:]
        lat = fin.variables['lat'][:]
        lon = fin.variables['lon'][:]

    # Calculate the average size of grid cells and the number of points
    # for a priori elimination of certain parameter combinations
    latr = lat*np.pi/180
    lonr = lon*np.pi/180
    dlatr = np.diff(latr)
    dlatr = np.concatenate((dlatr,[dlatr[-1]]))
    dlonr = np.diff(lonr)
    dlonr = np.concatenate((dlonr,[dlonr[-1]]))
    dlonr2d,dlatr2d = np.meshgrid(dlonr,dlatr)
    lonr2d,latr2d = np.meshgrid(lonr,latr)
    rearth = 6.371e6 # m
    cell_areas = rearth*np.cos(latr2d)*dlonr2d * rearth*dlatr2d
    Abar = np.average(cell_areas)
    Nt = len(latr)*len(lonr)

    # initialize the AR Detector
    ar_detector = ARDetector(lat,lon)

    # set  the number of points per sampling dimension
    ndim = 3
    dim_sizes = [128,128,128]
    num_points = np.prod(dim_sizes)

    smpi.pprint("Constructing parameter arrays")
    # set the parameter sampling cube

    # set the parameter values
    parameter_values = [np.linspace(pb[0],pb[1],dim_sizes[n]) for n,pb in enumerate(bayes.parameter_bounds)]

    theta_cubes = np.meshgrid(*parameter_values,indexing='ij')
    theta_cube = np.zeros((dim_sizes + [ndim]))
    for n in range(ndim):
        theta_cube[...,n] = np.array(theta_cubes[n],copy=True)
    del(theta_cubes)

    theta_cube_1d = theta_cube.reshape([-1,ndim])

    # do an inital check on the parameters
    parameter_mask = np.zeros(num_points,dtype=np.bool)
    parameter_mask = [ bayes.check_parameter_combo_validity(theta_cube_1d[n,:], Abar, Nt, False) for n in range(theta_cube_1d.shape[0])]

    # find indices of valid parameter combinations
    ivalid = np.nonzero(parameter_mask)[0]

    # construct the index, parameter combinations to scatter
    index_parameter_combo_master = [ (n, theta_cube_1d[n,:]) for n in ivalid ]
else:
    ar_count = None
    ar_detector = None
    IVT = None
    Abar = None
    Nt = None
    index_parameter_combo_master = None

if smpi.rank == 0:
    smpi.pprint("Scattering parameters")
# scatter the parameter list to participating processes
my_index_parameter_combo = smpi.scatterList(index_parameter_combo_master)

# broadcast additional information to participating processes
if smpi.rank == 0:
    smpi.pprint("Broadcasting auxillary data")
ar_count = smpi.comm.bcast(ar_count)
ar_detector = smpi.comm.bcast(ar_detector)
IVT = smpi.comm.bcast(IVT)
Abar = smpi.comm.bcast(Abar)
Nt = smpi.comm.bcast(Nt)

my_num_parameters = len(my_index_parameter_combo)
my_indices = np.array(list(zip(*my_index_parameter_combo))[0])
my_log_posterior = np.zeros(my_num_parameters)
my_min_counts = np.zeros(my_num_parameters)
my_max_counts = np.zeros(my_num_parameters)
my_avg_counts = np.zeros(my_num_parameters)

# pre-define an array 
tmp_counts = np.zeros(len(ar_count))

# loop over parameters
for i in range(my_num_parameters):

    if i % 10 == 0:
        smpi.pprint("Working on sample {}/{}".format(i,my_num_parameters))

    # extract the parameter combo
    theta = my_index_parameter_combo[i][1]

    # calculate the log likelihood and counts
    my_log_posterior[i] = bayes.log_posterior(theta,ar_count, IVT, ar_detector, False, Abar, Nt, count_array = tmp_counts)

    # store count statistics
    my_min_counts[i] = tmp_counts.min()
    my_max_counts[i] = tmp_counts.max()
    my_avg_counts[i] = tmp_counts.mean()

# gather statistics from all processes
smpi.pprint("Starting gather operation")
indices_master = smpi.comm.gather(my_indices)
log_posterior_master = smpi.comm.gather(my_log_posterior)
min_counts_master = smpi.comm.gather(my_min_counts)
max_counts_master = smpi.comm.gather(my_max_counts)
avg_counts_master = smpi.comm.gather(my_avg_counts)

# construct arrays out of all quantities
if smpi.rank == 0:

    # concatenate the gathered quantities into single 1D arrays
    indices_master = np.concatenate(indices_master)
    log_posterior_master = np.concatenate(log_posterior_master)
    min_counts_master = np.concatenate(min_counts_master)
    max_counts_master = np.concatenate(max_counts_master)
    avg_counts_master = np.concatenate(avg_counts_master)

    smpi.pprint("Constructing data cubes out of gathered info")

    # the posterior
    log_posterior_1d = -np.inf*np.ones(num_points)
    log_posterior_1d[indices_master] = log_posterior_master
    log_posterior = log_posterior_1d.reshape(dim_sizes)

    # the count statistics
    min_counts_1d = np.zeros(num_points)
    min_counts_1d[indices_master] = min_counts_master
    min_counts = min_counts_1d.reshape(dim_sizes)

    max_counts_1d = np.zeros(num_points)
    max_counts_1d[indices_master] = max_counts_master
    max_counts = max_counts_1d.reshape(dim_sizes)

    avg_counts_1d = np.zeros(num_points)
    avg_counts_1d[indices_master] = avg_counts_master
    avg_counts = avg_counts_1d.reshape(dim_sizes)

    # construct the output dictionary
    output_dictionary = {}
    output_dictionary['log_posterior'] = log_posterior
    output_dictionary['min_counts'] = min_counts
    output_dictionary['max_counts'] = max_counts
    output_dictionary['avg_counts'] = avg_counts
    output_dictionary['theta'] = theta_cube
    output_dictionary['dim_sizes'] = dim_sizes

    # add metadata to the dictionary
    output_dictionary = metadict.store_metadata_info(output_dictionary)

    smpi.pprint("Writing to disk")
    # write the dictionary to a pickle file
    with open('likelihood_and_counts_on_grid.pk','wb') as fout:
        pickle.dump(output_dictionary,fout)

    smpi.pprint("Job complete.")

