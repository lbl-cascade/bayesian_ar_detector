#!/usr/bin/env python
import matplotlib as mpl
import pylab as PP
import pickle
import fastkde.plot
import numpy as np

with open('bayes_sampler_v9.pk','rb') as fin:
    input_dict = pickle.load(fin)

input_dict.print_props()

sampler_chain = input_dict['sampler_chain']

plot_trace = True
if plot_trace:
    fig, ax = PP.subplots(sampler_chain.shape[-1],1)

    for i in range(sampler_chain.shape[-1]):
        for n in range(0,sampler_chain.shape[0]):
            x_vals = np.arange(sampler_chain.shape[1])
            y_vals = np.ma.masked_equal(sampler_chain[n,:,i],0.0)
            ax[i].plot(x_vals,y_vals,'k-',alpha=0.05)

    ax[-2].set_yscale('log')
    ax[-1].set_yscale('log')
    PP.show()

plot_pairs = True
if plot_pairs:
    #nburn = 1340 
    ndim = input_dict['ndim']
    nburn = input_dict['nburn']
    #i = input_dict['current_step']-10
    #parameter_trace = np.ma.masked_equal(sampler_chain[:, i, :],0.0).compressed().reshape(-1, ndim).T
    parameter_trace = input_dict['sampler_last_position'].reshape(-1,ndim).T

    if ndim == 4:
       var_names = ['P', r'$\Delta$y', r'A$_{min}$',r'$\sigma$']
       log_scale = [False,False,False,False]
    else:
       var_names = ['P', r'$\Delta$y', r'A$_{min}$']
       log_scale = [False,False,False]

    fastkde.plot.pair_plot(parameter_trace[:,:], \
                           draw_scatter=True, \
                           var_names = var_names, \
                           log_scale = log_scale)


