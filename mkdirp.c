#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <unistd.h>

//A pure c implementation of mkdir -p functionality
//based on code from 
//http://nion.modprobe.de/blog/archives/357-Recursive-directory-creation.html
// (http://nion.modprobe.de/tmp/mkdir.c
void mkdirp(const char *path)
{
        char opath[256];
        char *p;
        size_t len;

        strncpy(opath, path, sizeof(opath));
        len = strlen(opath);
        if(opath[len - 1] == '/')
                opath[len - 1] = '\0';
        for(p = opath; *p; p++)
                if(*p == '/') {
                        *p = '\0';
                        if(access(opath, F_OK))
                                mkdir(opath, S_IRWXU);
                        *p = '/';
                }
        if(access(opath, F_OK))         /* if path is not terminated with / */
                mkdir(opath, S_IRWXU);
}


/*
void main (void){

mkdir_p ("/home/nion/irc/freenode/grml/laldsa/asd");
}
*/
