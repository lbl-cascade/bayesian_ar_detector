#!/usr/bin/env python
"""
Draws samples from the posterior distribution of AR detector parameters,
given a database of manual counts.
"""
import os
import socket
import pickle
import sys
import netCDF4 as nc
import numpy as np
import xarray as xr
import bayesian_count_model as bayes
import metadict


def run_bayesian_model( num_walkers = 8,
                        num_steps = 1000,
                        steps_per_save = 10,
                        emcee_a_parameter = 2,
                        input_sampler_file = None,
                        output_sampler_file = "mcmc_samples.nc",
                        be_verbose = True,
                        use_sigma = True):
    
    def vprint(msg):
        if be_verbose:
            print(msg)

    if use_sigma:
        num_parameters = 4
    else:
        num_parameters = 3
        
    # initialize MPI
    pool = None
    pool = MPIPool()
    #pool.debug = True


    #******************
    # Worker processes
    #******************

    # for pool workers, just sit here until the emcee parallelization begins;
    # end afterward
    if pool is not None:
        if not pool.is_master():
            pool.wait()
            sys.exit(0)

    #****************
    # Master process
    #****************

    vprint("Loading the IVT and count data")
    # load the count file
    #ar_count_file = '/project/projectdirs/m1517/cascade/taobrien/artmip/tier1/ar_count_interface/ivt_rgb_images_percentile_counts.nc'
    with nc.Dataset(ar_count_file) as fin:
        ar_count = fin.variables['AR_COUNT'][:]
        lat = fin.variables['lat'][:]
        lon = fin.variables['lon'][:]

    # Calculate the average size of grid cells and the number of points
    # for a priori elimination of certain parameter combinations
    rearth = 6.371e6
    latr = lat*np.pi/180
    lonr = lon*np.pi/180
    dlatr = np.diff(latr)
    dlatr = np.concatenate((dlatr,[dlatr[-1]]))
    dlonr = np.diff(lonr)
    dlonr = np.concatenate((dlonr,[dlonr[-1]]))
    dlonr2d,dlatr2d = np.meshgrid(dlonr,dlatr)
    lonr2d,latr2d = np.meshgrid(lonr,latr)
    cell_areas = rearth*np.cos(latr2d)*dlonr2d * rearth*dlatr2d
    Abar = np.average(cell_areas)
    Nt = len(latr)*len(lonr)

    vprint("Setting initial positions")

    # set the starting guesses to be somewhere within the reasonable parameter space
    parameter_bounds = bayes.parameter_bounds
    if use_sigma:
        parameter_bounds.append([0.5,4])

    if input_sampler_file is not None: 
        input_ds = xr.open_dataset(input_sampler_file)

        vprint(input_ds)

        # get the step of the last run (used for restarting from)
        # a run that was midway done
        step_end_last_run = input_ds.attrs['last_run_step']

        # get the sampler's last state
        starting_guesses = np.array(input_ds['sampler_state'].data[step_end_last_run,:,:], copy = True)
        sampler_lnprob   = np.array(input_ds['sample_lnprob'].data[step_end_last_run,:], copy = True)
        sampler_rstate = None

        # close the sampler file
        input_ds.close()
        del(input_ds)
    else:
        np.random.seed(0)
        starting_guesses = np.zeros([num_walkers, num_parameters])
        for bounds, n in zip(parameter_bounds, range(num_parameters)):
            starting_guesses[:,n] = np.random.uniform(low=bounds[0], high = bounds[1], size = num_walkers)

        # check that that the starting guesses produce valid results
        # and keep generating samples until all do
        still_have_bad_values = True
        while still_have_bad_values:
            # Find any parameter combinations that are invalid
            ibad = np.nonzero( [not bayes.check_parameter_combo_validity(starting_guesses[n,:], Abar, Nt, use_sigma) for n in range(num_walkers) ])[0]
            if len(ibad) == 0:
                still_have_bad_values = False
                break
            else:
                for bounds, n in zip(parameter_bounds, range(num_parameters)):
                    new_guesses = np.random.uniform(low=bounds[0], high = bounds[1], size = len(ibad))
                    starting_guesses[ibad,n] = new_guesses


        sampler_last_position = None
        sampler_lnprob = None
        sampler_rstate = None


    # run the MCMC sampler
    vprint("Initializing emcee with {} walkers".format(num_walkers))
    sampler = emcee.EnsembleSampler(num_walkers,
                                    num_parameters,
                                    bayes.log_posterior,
                                    a = emcee_a_parameter,
                                    args=[ar_count, use_sigma, Abar, Nt],
                                    pool = pool)


    #*******************************************************************************
    # initialize an xarray dataset to hold the sampler state information
    #*******************************************************************************
    walkers = xr.DataArray(np.arange(num_walkers), dims = "walker")
    steps = xr.DataArray(np.arange(num_steps), dims = "step")
    parameters = xr.DataArray(np.arange(num_parameters), dims = "parameter")

    sampler_state_da = xr.DataArray(np.ma.masked_all([num_steps, num_walkers, num_parameters]),
                               dims = ('step', 'walker', 'parameter'),
                               coords = dict(step = steps, walker = walkers, parameter = parameters))
    sampler_state_da.attrs['long_name'] = 'MCMC Sampler Chain State'

    sampler_lnprob_da = xr.DataArray(np.ma.masked_all([num_steps, num_walkers]),
                               dims = ('step', 'walker'),
                               coords = dict(step = steps, walker = walkers))
    sampler_lnprob_da.attrs['long_name'] = 'Log of MCMC posterior probability of the state'

    acor_array_da = xr.DataArray(np.ma.masked_all([num_steps, num_parameters]),
                               dims = ('step', 'parameter'),
                               coords = dict(step = steps, parameter = parameters))
    acor_array_da.attrs['long_name'] = 'MCMC Sampler Autocorrelation'

    af_array_da = xr.DataArray(np.ma.masked_all([num_steps, num_walkers]),
                               dims = ('step', 'walker'),
                               coords = dict(step = steps, walker = walkers))
    af_array_da.attrs['long_name']  = 'MCMC Sampler Acceptance Fraction'

    output_ds = xr.Dataset(dict(
                                sampler_state = sampler_state_da,
                                sample_lnprob = sampler_lnprob_da,
                                acor = acor_array_da,
                                acceptance_fraction = af_array_da,
                               ))
    output_ds.attrs['git_sha'] = str(metadict._git_sha)
    output_ds.attrs['git_branch'] = str(metadict._git_branch)
    output_ds.attrs['hostname'] = str(socket.gethostname())
    output_ds.attrs['current_working_directory'] = os.getcwd()
    output_ds.attrs['command'] = " ".join(sys.argv)


    # iteratively run the sampler and save progress
    for i, result in enumerate(sampler.sample(starting_guesses, iterations = num_steps, lnprob0 = sampler_lnprob, rstate0 = sampler_rstate)):

        try:
            acor = sampler.acor
        except:
            acor = np.zeros([num_parameters])
        try:
            acceptance_fraction = sampler.acceptance_fraction
        except:
            acceptance_fraction = np.zeros([num_parameters])

        output_ds['sampler_state'].data[i,:,:] = result[0]
        output_ds['sample_lnprob'].data[i,:] = result[1]
        output_ds['acor'].data[i,:] = acor
        output_ds['acceptance_fraction'].data[i,:] = acceptance_fraction
        output_ds.attrs['last_run_step'] = i

        # write the file to disk if requested
        if i % steps_per_save == 0 or i == num_steps - 1:

            vprint("Step {:04} of {:04}, acor = {}, acceptance_fraction = {}".format(i,num_steps,np.average(acor), np.average(acceptance_fraction)))
            output_ds.to_netcdf(output_sampler_file)



    pool.close()
    vprint("MCMC calculation done.")

    
if __name__ == "__main__":
    
    import emcee
    from emcee.utils import MPIPool
    import mpi4py
    
    import argparse
    argparser = argparse.ArgumentParser(epilog=__doc__)
    argparser.add_argument('inputcounts',help="Input AR count file")
    argparser.add_argument('--outputfile','-o',default=None,help="File to which to write the MCMC sample state")
    argparser.add_argument('--inputsamplefile',default=None,help="Input MCMC sample state file from which to initialize")
    argparser.add_argument('--num_threads','-t',default=1,help="Number of threads (-1 = automatically determine)")
    argparser.add_argument('--num_walkers',default=-1,help="Number of walkers (-1 = automatically determine)")
    argparser.add_argument('--num_steps','-n',default=1000,help="Number of MCMC steps to run")
    argparser.add_argument('--steps_per_save',default=10,help="Number of MCMC steps between checkpoint writes")
    argparser.add_argument('--emcee_a',default=2,help="emcee scale parameter (see http://dfm.io/emcee/current/api/#the-affine-invariant-ensemble-sampler)")
    argparser.add_argument('--quiet',default=False,action = 'store_true',help="flag whether to turn off verbosity")
    argparser.add_argument('--use_sigma',default=True,action = 'store_false',help="flag whether to integrate over the sigma parameter in the Bayesian model")

    parsed_args = vars(argparser.parse_args())
    
    # input count file
    ar_count_file = parsed_args['inputcounts']
    
    # output MCMC state file
    output_sampler_file = parsed_args['outputfile']
    if output_sampler_file is None:
        output_sampler_file = "mcmc_samples_" + os.path.basename(ar_count_file)
        
    input_sampler_file = parsed_args['inputsamplefile']
        
    # number of walkers
    num_walkers = int(parsed_args['num_walkers'])
    if num_walkers == -1:
        num_mpi = mpi4py.MPI.COMM_WORLD.Get_size()
        num_walkers = num_mpi
        
    # threads
    thread_pool_size = int(parsed_args['num_threads'])
        
    # MCMC parameters
    num_steps = int(parsed_args['num_steps'])
    steps_per_save = int(parsed_args['steps_per_save'])
    emcee_a_parameter = parsed_args['emcee_a']
    be_verbose = not parsed_args['quiet']
    use_sigma = parsed_args['use_sigma']
        
     
    # initialize the AR detector
    comm = mpi4py.MPI.COMM_SELF
    bayes.ar_detector = bayes.TECAMERRAARCounter.TECAMERRAARCounter(ar_count_file,
                                                                    no_time_var = False,
                                                                    comm = comm,
                                                                    thread_pool_size = thread_pool_size)

    # run the bayesian model
    run_bayesian_model( num_walkers = num_walkers,
                        num_steps = num_steps,
                        steps_per_save = steps_per_save,
                        emcee_a_parameter = emcee_a_parameter,
                        input_sampler_file = input_sampler_file,
                        output_sampler_file = output_sampler_file,
                        be_verbose = be_verbose,
                        use_sigma = use_sigma)
