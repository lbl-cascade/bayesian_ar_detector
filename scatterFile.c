#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

void mkdirp(const char *);

//Handle and check MPI errors.  Abort properly if necessary.
void handleAndCheck(char * caFileName,
                    int iLine,
                    int mpiStatus)
{
  char * caMPIMessage;
  int resultlen;
  if(mpiStatus != MPI_SUCCESS){
    MPI_Error_string(mpiStatus,caMPIMessage,&resultlen);
    fprintf(stderr,"Error (%s:%d): %s\n",caFileName,iLine,caMPIMessage);
    MPI_Abort(MPI_COMM_WORLD,1);
  }
}

void printUsage(char * progname)
{
  printf(" usage: %s FILETOCOPY DESTDIR\n \n Uses mpi to read FILETOCOPY with the root process and broadcast it to\n the rest of the processes for writing to DESTDIR. \n \n NOTE: To avoid write conflicts during the untar step, this should only be run\n once for each node.  This can be achieved by running with only as many\n processes as there are nodes and indicating to the mpirun/srun command that we\n only want one processing element per node. E.g. for 8 nodes:\n \n mpirun -n 8 -npernode 1 scatterFile mytarball\n \n or\n \n srun --ntasks=8 --ntasks-per-node=1 scatterFile mytarball\n \n ",progname);
}

int main(int argc, char * argv[]){

  int ioresult = 0;
  //Input
  const char * caInFileName = "test.tar";
  FILE * pInputFile;
  //Output
  FILE * pOutputFile;
  char caOutFileName[500]; 
  char * caOutDir; 

  //Buffer sizes
  long long lFileSize,lChunkSize;

  /*****************************************************************************
   * Initialize MPI
   ****************************************************************************/
  int rank, size;
  int mpiStatus;
  MPI_Init(&argc, &argv); //Initialize MPI
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); //Get the processor ID
  MPI_Comm_size(MPI_COMM_WORLD, &size); //Get the processor count

  //Check that a file was given as the only allowable argument
  if(argc == 3){
    caInFileName = argv[1];
    caOutDir = argv[2];
  }else{
    printUsage(argv[0]);
    MPI_Abort(MPI_COMM_WORLD,1);
  }

  //Have each processor make the output directory
  mkdirp(caOutDir);

  //Open the input file, get its file size, and transmit to the other procs
  if(rank == 0){

    pInputFile = fopen(caInFileName,"rb");
    if(pInputFile == NULL){
        fprintf(stderr,"Error reading %s\n",caInFileName);
        MPI_Abort(MPI_COMM_WORLD,1);
    }

    //Get the file size
    fseek(pInputFile,0,SEEK_END);
    lFileSize = ftell(pInputFile);
    rewind(pInputFile);

    //Set the file chunk size (in bytes)
    lChunkSize= 10485760LL;
    if(lChunkSize > lFileSize){lChunkSize = lFileSize;}
  }

  /*****************************************************************************
   * Initialize the transfer
   ****************************************************************************/
  //Open the output file
  sprintf(caOutFileName,"%s/%s\0",caOutDir,caInFileName); 
  pOutputFile = fopen(caOutFileName,"wb");
  if(pOutputFile == NULL){
      fprintf(stderr,"Error creating %s\n",caOutFileName);
      MPI_Abort(MPI_COMM_WORLD,1);
  }


  // Broadcast the file size to all processors
  handleAndCheck( __FILE__,__LINE__,
                  MPI_Bcast(  &lFileSize,1,MPI_LONG_LONG, 0,MPI_COMM_WORLD) );

  // Broadcast the chunk size to all processors
  handleAndCheck( __FILE__,__LINE__,
                  MPI_Bcast(  &lChunkSize,1,MPI_LONG_LONG, 0,MPI_COMM_WORLD) );
 

  //Have each processor state the file length
  printf("(%d of %d): Ready to process file of size %d in chunks of %d\n",rank,size,lFileSize,lChunkSize);

  //Allocate memory for the send/receive chunks
  unsigned char * fileBuffer = (unsigned char *) malloc(lChunkSize*sizeof(char));
  if(fileBuffer == NULL){
    fprintf(stderr,"(Rank %d, (%s:%d): Memory allocation error\n",rank,__FILE__,__LINE__);
    MPI_Abort(MPI_COMM_WORLD,1);
  }


  /*****************************************************************************
   * Exceute the transfer
   ****************************************************************************/
  //Calculate the number of loops to complete the transfer
  long nloops = lFileSize/lChunkSize;
  long long lLastChunk = lFileSize % lChunkSize;
  long lChunkDum = 0;

  //Loop through and transfer the file
  for(int j = 0; j <= nloops; j++) {
    if((j < nloops) || (j == nloops && lLastChunk != 0)){
      if(j < nloops){
        lChunkDum = lChunkSize;
      }
      else{
        lChunkDum = lLastChunk;
      }

      if( rank == 0){
        ioresult = fread(fileBuffer,sizeof(char),lChunkDum,pInputFile);
        if(ioresult != lChunkDum){
          fprintf(stderr,"Error reading %s\n",caInFileName);
          MPI_Abort(MPI_COMM_WORLD,1);
        }
      }

      //Broadcast the file contents
      handleAndCheck( __FILE__,__LINE__,
                      MPI_Bcast(  fileBuffer,lChunkDum,MPI_BYTE,0,MPI_COMM_WORLD) );

      //Write the file to disk
      fwrite(fileBuffer,sizeof(char),lChunkDum,pOutputFile);
    }
  }
  if(rank == 0) { fclose(pInputFile); } //Close the input file
  fclose(pOutputFile);//Close the output file
  free(fileBuffer); //Free memory
  
  /*****************************************************************************
   * Take down MPI
   ****************************************************************************/
  MPI_Finalize();

  //Return and end;
  return 0;
  
}
