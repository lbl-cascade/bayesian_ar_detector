#!/usr/bin/env python
# coding: utf-8
import netCDF4 as nc
import numpy as np
import BayesianARDetector
from simplempi import simpleMPI

# initialize MPI
smpi = simpleMPI.simpleMPI()

# read in the IVT dataset
dimensions = ['time','lat','lon']

if smpi.rank == 0:
    smpi.pprint('Reading the count file')
    ar_count_file = '/project/projectdirs/m1517/cascade/taobrien/artmip/tier1/ar_count_interface/front_ivt_iwv_images_counts.nc'

    dim_vars = {}
    dim_atts = {}
    with nc.Dataset(ar_count_file) as fin:
        IVT = fin.variables['IVT'][:]
        ar_count = fin.variables['AR_COUNT'][:]
        for dim in dimensions:
            dim_vars[dim] = np.array(fin.variables[dim][:],copy=True)
            dim_atts[dim] = { att : fin.variables[dim].getncattr(att) \
                                    for att in fin.variables[dim].ncattrs()}
        dates = nc.num2date(dim_vars['time'],dim_atts['time']['units'])


    IVT_dict = { n : IVT[n,:,:] for n in range(IVT.shape[0]) }
    
    smpi.pprint('Broadcasting and scattering variables')

else:
    dim_vars = {dim: None for dim in dimensions}
    dim_atts = {dim: None for dim in dimensions}
    IVT_dict = None
    ar_counts = None
    ar_probs = None


# broadcast the lat/lon values
lat = smpi.comm.bcast(dim_vars['lat'],0)
lon = smpi.comm.bcast(dim_vars['lon'],0)
    
# scatter the list of MPI fields
my_IVT_dict = smpi.scatterList(IVT_dict)

# initialize the AR detector
ar_detector = BayesianARDetector.BayesianARDetector(lat,lon)


my_ar_probs = {}
my_ar_counts = {}
# run the detector
for n, IVT_field in my_IVT_dict.items():
    smpi.pprint('Running the detector on field {}'.format(n))
    ar_probs_tmp, ar_counts_tmp = ar_detector.probabilistic_detect_ars(IVT_field)
    # store the results
    my_ar_probs[n] = np.array(ar_probs_tmp,copy=True)
    my_ar_counts[n] = np.array(ar_counts_tmp,copy=True)


# gather the results
smpi.pprint('Starting the gather operation')
ar_probs = smpi.comm.gather(my_ar_probs)
ar_counts = smpi.comm.gather(my_ar_counts)

if smpi.rank == 0:
    # unpack the gathered list of dictionaries into single dictionaries
    ar_probs_dict = {}
    ar_counts_dict = {}
    for item in ar_probs:
        for key in item:
            ar_probs_dict[key] = item[key]
    for item in ar_counts:
        for key in item:
            ar_counts_dict[key] = item[key]

    ar_probs = ar_probs_dict
    ar_counts = ar_counts_dict


# Write the results to disk
if smpi.rank == 0:
    smpi.pprint('Writing results to disk')
    # set the max count
    count_max = 32

    output_file_name = ar_detector.posterior_file.replace('.pk','.nc')

    with nc.Dataset(output_file_name,'w') as fout:
        # create dimension/dimension variables
        for vname in dimensions:
            var = dim_vars[vname]
            fout.createDimension(vname, len(var))
            fout.createVariable(vname,var.dtype.str,(vname,))

            # set attributes
            for att in dim_atts[vname]:
               fout.variables[vname].setncattr(att, dim_atts[vname][att])

        # create the count variable
        fout.createDimension('count',count_max+1)
        vcount = fout.createVariable('count','i4',('count',))
        vcount.long_name = 'Number of counted ARs'
        vcount.units = 'number'

        vprob = fout.createVariable('ar_prob','f4',tuple(dimensions))
        vhist = fout.createVariable('ar_count_histogram','i4',('time','count'))

        vprob.long_name = 'Posterior AR Probability'
        vprob.units = 'fraction'
        vhist.long_name = 'Posterior Histogram of AR counts'
        vhist.units = 'number'

        # store dimension variables
        for vname in dimensions:
            fout.variables[vname][:] = dim_vars[vname]

        # store the counts
        ar_numbers = np.arange(count_max+1,dtype=int)
        fout.variables['count'][:] = ar_numbers
        
        # store ar probabilities and counts
        for n in sorted(ar_probs):
            fout.variables['ar_prob'][n,:,:] = ar_probs[n]
            fout.variables['ar_count_histogram'][n,:] = np.array([ len(np.nonzero(ar_counts[n] == i)[0]) for i in ar_numbers])



