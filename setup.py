from distutils.core import setup,Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext
import Cython.Compiler.Options
Cython.Compiler.Options.annotate = True
import numpy

inc_dirs = ['floodfillsearch/src/']
inc_dirs.append(numpy.get_include())
lib_dirs = ['floodfillsearch/bld/']
libs = ['libconnectedcomponentnd']
#lib_dirs.append(numpy.get_lib())

cmdclass = {'build_ext': build_ext}

extensions = [Extension("damped_zonal_anomaly", \
                        ["damped_zonal_anomaly.pyx"], \
                        language='c++', \
                        extra_compile_args = ["-std=c++14","-fopenmp","-O3", "-ffast-math"], \
                        extra_link_args = ["-fopenmp"], \
                        library_dirs=lib_dirs, \
                        include_dirs=inc_dirs, \
                        runtime_library_dirs=lib_dirs), \
              Extension("BayesianARDetection", \
                        ["BayesianARDetection.pyx"], \
                        libraries=libs, \
                        language='c++', \
                        extra_compile_args = ["-std=c++14","-fopenmp","-O3", "-ffast-math"], \
                        extra_link_args = ["-fopenmp"], \
                        library_dirs=lib_dirs, \
                        include_dirs=inc_dirs, \
                        runtime_library_dirs=lib_dirs)]

revision = "1.0"

setup(
            name = 'damped_zonal_anomaly', \
            version = revision, \
            description = 'A tool for finding connected components', \
            author = "Travis A. O'Brien", \
            author_email = "TAOBrien@lbl.gov", \
            url = "https://bitbucket.org/lbl-cascade/floodfillsearch", \
            download_url = "https://bitbucket.org/lbl-cascade/floodfillsearch/get/v{}.tar.gz".format(revision), \
            keywords = ['connected component','flood'], \
            classifiers = [], \
            cmdclass = cmdclass,\
            ext_modules = extensions, \
            )
