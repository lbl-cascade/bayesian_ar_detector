#!/bin/bash

NUM_BATCH=5
SCRIPT='run_sampler.sbatch'


for i in `seq 1 $NUM_BATCH`
do

    if [ "$i" -eq "1" ]; then
        SBATCH_COMMAND="sbatch $SCRIPT"
    else
        SBATCH_COMMAND="sbatch --dependency=afterok:$job_id $SCRIPT"
    fi
    echo $SBATCH_COMMAND
    SLURM_OUTPUT=`$SBATCH_COMMAND`
    job_id=`echo $SLURM_OUTPUT | sed 's/Submitted\ batch\ job\ '//`
    echo $job_id >> active_jobs.list
done
