PROJECTNAME = scatterFile

ifneq ("${NERSC_HOST}","")
  CXX = CC
  CC = cc
  FC = ftn
else
  CXX=mpiCC
  CC=mpicc
  FC=mpif90
endif
LD = $(CXX)

#DFLAGS = -DVERBOSE
CXXFLAGS =  -g -ansi -pedantic -Wall $(DFLAGS)
#CXXFLAGS =  -g $(DFLAGS)
CFLAGS = $(CXXFLAGS)
FFLAGS = $(CFLAGS)

SRC := $(wildcard *.c *.cc *.F90)
OBJ := $(addsuffix .o, $(basename $(SRC)))

all: main

%.o: %.c
	$(CXX) $(CXXFLAGS) -c $< -o $@

%.o: %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

%.o: %.F90
	$(FC) $(FFLAGS) -c $< -o $@
	
main: $(OBJ)
	$(LD) $(CXXFLAGS) -o $(PROJECTNAME) $(OBJ)

clean:
	-rm -f ./*.o ./*.a

